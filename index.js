const express = require('express');
const app = express();
const server = require('https').createServer(app);
const path = require('path');
const ENV = require('dotenv');
var cors = require('cors');
const csvParser = require('./routes/csv');
ENV.config();
const porta = process.env.PORT || 3000;

// General Setup
app.use(express.json()); // to support JSON-encoded bodies
app.use(express.urlencoded({ // to support URL-encoded bodies
    extended: true
}));
app.use(express.static(__dirname + '/public'));
//---

// Handlebars
const handleBars = require('express-handlebars');
app.set('views', path.join(__dirname, '/views'));
app.engine('handlebars', handleBars({
    defaultLayout: "index",
    helpers: {
        is: function (v1, operator, v2, options) {
            switch (operator) {
                case '==':
                    return (v1 == v2) ? options.fn(this) : options.inverse(this);
                case '===':
                    return (v1 === v2) ? options.fn(this) : options.inverse(this);
                case '!=':
                    return (v1 != v2) ? options.fn(this) : options.inverse(this);
                case '!==':
                    return (v1 !== v2) ? options.fn(this) : options.inverse(this);
                case '<':
                    return (v1 < v2) ? options.fn(this) : options.inverse(this);
                case '<=':
                    return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                case '>':
                    return (v1 > v2) ? options.fn(this) : options.inverse(this);
                case '>=':
                    return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                case '&&':
                    return (v1 && v2) ? options.fn(this) : options.inverse(this);
                case '||':
                    return (v1 || v2) ? options.fn(this) : options.inverse(this);
                default:
                    return options.inverse(this);
            }
        }
    },
}));
app.set('view engine', 'handlebars');
//---

//All Routes and 404 page for now only render HomePage
app.get('/home', function(request, response) {
    response.render('home', {
        style: [],
        javascript: []
    });
});

app.get('/getLog', cors(), async function (request, response) {
    await csvParser.parserCsv(request, response)
    .then(() =>{
        csvParser.getRanking()
        .then((result) => {
            response.status(200).send(result);
        })
    })
    .catch(() => {
        response.status(400).send({
            error: "error parser csv"
        });
    });
})

//All Routes and 404 page for now only render HomePage
app.get('*', function (request, response) {
    response.render('home', {
        style: [],
        javascript: []
    });
});

app.listen(porta, function () {
    console.log("server on in: " + porta)
});