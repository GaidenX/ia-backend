exports.createRanking = function (rankingArray) {
    const orderRanking = function (a, b) {
        return b.type - a.type;
    }
    
    return rankingArray.sort(orderRanking);
}