const csv = require('csv-parser');
const fs = require('fs');
const axios = require('axios');
const logDnaCsv = './data/error_messages.csv';
const helpers = require('./helpers');
const results = {
    logs: []
};

var options = {
    host: 'https://logs-ml-aioncloud.mybluemix.net',
    path: '/predict'
};

exports.parserCsv = function (request, response) {
    return new Promise((resolve, reject) => {
        if (results.logs.length > 1) {
            resolve(results.logs);
        } else {
            fs.createReadStream(logDnaCsv)
                .pipe(csv())
                .on('data', (data) => {
                    let message = "";
                    Object.keys(data).forEach((key) => {
                        message += data[key];
                    })
                    results.logs.push(message);
                })
                .on('end', () => {
                    resolve(results);
                });
        }
    });
}

exports.getRanking = function () {
    return new Promise((resolve, reject) => {
        axios.post('https://logs-ml-aioncloud.mybluemix.net/predict', results)
            .then(function (response) {
                let rankingArray = [];

                Object.keys(response.data).forEach((key) => {
                    rankingArray.push({
                        type: response.data[key],
                        message: results.logs[key]
                    })
                });

                helpers.createRanking(rankingArray);
                resolve(rankingArray);
            })
            .catch(function (error) {
                reject(error);
            });
    });
}